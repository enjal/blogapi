# Application controller for this app
class ApplicationController < ActionController::API

  # Error Handling when route is not present.
  rescue_from StandardError do |e|
    error(e)
  end

  def routing_error
    raise ActionController::RoutingError, params[:path]
  end

  protected

  def error(e)
    error_info = {
      error: 'internal-server-error',
      exception: "#{e.class.name} : #{e.message}"
    }
    render json: error_info.to_json, status: 500
  end
end
