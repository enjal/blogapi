# Defining Posts controller here
class PostsController < ApplicationController
  def index
    response = {}
    Post.order('id').each_with_index do |post, i|
      response[i] = {}
      response[i]['post'] = post
      response[i]['comments'] = post.comments
    end

    render json: response.to_json
  end

  def edit
  end

  def update
    post = Post.find(params[:id])
    if post.update(post_params)
      render json: { status: 'success' }
    else
      render json: { status: 'failed', reasons: post.errors.full_messages }
    end
  rescue => e
    render json: { status: 'error', reasons: e.message }
  end

  # Method for handling creation of posts
  def create
    post = Post.new(post_params)
    if post.save
      render json: { status: 'success' }
    else
      render json: { status: 'failed', reasons: post.errors.full_messages }
    end
  rescue => e
    render json: { status: 'error', reasons: e.message }
  end

  def new
  end

  # Methods for handling deletion of posts
  def destroy
    post = Post.find(params[:id])
    if post.destroy
      render json: { status: 'success' }
    else
      render json: {
        status: 'failed', failed_reason: post.errors.full_messages
      }
    end
  rescue => e
    render json: { status: 'error', error: e.message }
  end

  def show
  end

  private
  # Set validation for post parameters
  def post_params
    params.require(:parameters).permit(:heading, :content)
  end
end
