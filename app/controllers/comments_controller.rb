# Defining Posts controller here
class CommentsController < ApplicationController
  def index
    response = {}
    Comment.order('id').each_with_index do |comment, i|
      response[i] = {}
      response[i]['comment'] = comment      
    end
    render json: response.to_json
  end

  def edit
  end

  # Method for handling update of comments.
  def update
    comment = Comment.find(params[:id])
    if comment.update(post_params)
      render json: { status: 'success' }
    else
      render json: { status: 'failed', reasons: comment.errors.full_messages }
    end
  rescue => e
    render json: { status: 'error', reasons: e.message }
  end

  #Method for handling creation of comments
  def create    
    comment = Comment.new(post_params)
    if comment.save
      render json: { status: 'success' }
    else
      render json: { status: 'failed', reasons: comment.errors.full_messages }
    end
  rescue => e
    render json: { status: 'error', reasons: e.message }
  end
   

  def new
  end

  # Method for handling deletion of comments
  def destroy
    comment = Comment.find(params[:id])
    if comment.destroy
      render json: { status: 'success' }
    else
      render json: {
        status: 'failed', failed_reason: comment.errors.full_messages
      }
    end
  rescue => e
    render json: { status: 'error', error: e.message }
  end 
  
  def show
  end

  private

  # Set required validation for post parameters 
  def post_params
    params.require(:parameters).permit(:post_id, :content)
  end 

end
