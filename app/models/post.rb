class Post < ActiveRecord::Base
  validates_presence_of :heading
  validates_presence_of :content

  has_many :comments, dependent: :destroy

end
