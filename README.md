# README #

This README would show how the api works for creation of comments.

### How do I create a comment? ###
* First use this command to create the post :

```sh
curl -i -H "Accept: application/json" -X POST -d "parameters[heading]=Article Heading&parameters[content]=Article Content" http://localhost:3000/posts 

```
* Now we can create comment. For creating of comment we use the following curl command:
```sh
curl -i -H "Accept: application/json" -X POST -d "parameters[post_id]=1&parameters[content]=Article Comment" http://localhost:3000/comments
```
### Response returned while creating comment ###
```sh
HTTP/1.1 200 OK 
X-Frame-Options: SAMEORIGIN 
X-Xss-Protection: 1; mode=block 
X-Content-Type-Options: nosniff 
Content-Type: application/json; charset=utf-8 
Etag: W/"5820854f62a6eb3d38ba7ba0d1b3ea75" 
Cache-Control: max-age=0, private, must-revalidate 
X-Request-Id: 7e7c78f0-2ab6-4da5-bfab-e53a4e9cfe42 
X-Runtime: 0.697861 
Server: WEBrick/1.3.1 (Ruby/2.2.2/2015-04-13) 
Date: Mon, 02 May 2016 15:30:22 GMT 
Content-Length: 20 
Connection: Keep-Alive

{"status":"success"}
```